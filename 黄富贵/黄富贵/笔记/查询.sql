--查询公司员工所在城市（不需要重复数据）
--distinct
select distinct PeopleAddress from People 

--查询所有员工信息(根据工资排序(order by)，降序排列)
select * from People order by PeopleSalary desc


--查询1980年以后出生的员工信息
select * from People where year(PeopleBirth)>=1980


--查询出巨蟹 6.22--7.22 的员工信息

--查询出和赵云在同一个城市的人


select * from People where PeopleAddress=(select peopleAddress from People where PeopleName='赵云')

--查询出电话号码开头138的员工信息,第4位可能是7，可能8 ，最后一个号码是5
select * from people
where PeoplePhone like '138[7-9]%5'  --789

--查询出电话号码开头133的员工信息,第4位是2-5之间的数字 ，最后一个号码不是2和3
select * from people
where PeoplePhone like '138[2-5]%[^23]'


--求出年龄比平均年龄高的人员信息
--平均年龄：今年-出生年份
select avg(year(getdate())-year(peoplebirth)) 平均年龄 from People

select * from people where (year(getdate())-year(peoplebirth))>(select avg(year(getdate())-year(peoplebirth)) 平均年龄 from People)

--平均工资
select avg(PeopleSalary) 平均工资 from People

--保留小数点后2位
--round(小数,保留位数)
select round(avg(PeopleSalary),2) 平均工资 from People

--convert 02154.13200 decimal(5,2):保留小数后2位，有效数字总共是5

--convert和cast 可以强制转换数据类型
select convert(decimal(13,2),avg(PeopleSalary)) 平均工资 from People

--cast
select cast(avg(PeopleSalary) as decimal(13,2)) 平均工资 from People


--根据员工所在地区分组统计员工人数，员工工资总和，平均工资，最高工资和最低工资，
--要求筛选出员工人数至少在2人及以上的记录，并且1985年及以后出身的员工不参与统计。
--count(*):空值也统计 count(字段)：当前字段的空值不统计
select count(*) 员工人数,sum(PeopleSalary) 工资总和, max(PeopleSalary) 最高工资,min(PeopleSalary) 最低工资 from People 
where year(PeopleBirth)<1985
group by PeopleAddress
having count(*)>=2


--删除数据
delete from People where PeopleName='蜗牛' 

--删除
--drop 删除整张表
--truncate删除整张表，表还在

--根据部门分组统计员工人数，员工工资总和，平均工资，最高工资和最低工资，平均工资在10000 以下的不参与统计，并且根据平均工资降序排列。
select count(*) 员工人数, sum(PeopleSalary) 员工工资总和,convert(decimal(15,2),avg(PeopleSalary)) 平均工资 from Department de
join People po on de.DepartmentId = po.DepartmentId
group by po.DepartmentId having avg(PeopleSalary)>=10000
order by 平均工资 desc

create table B(
	cid int primary key,
	cname varchar(20)
)


create table A(
	sid int,
	sname varchar(10),
	cid int references B(cid)
)

select * from A 
left join B on A.cid=B.cid