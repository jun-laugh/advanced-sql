--1. 查询出武汉地区所有的员工信息，要求显示部门名称以及员工的详细资料
SELECT p.*,d.DepartmentName FROM People p
INNER JOIN Department d ON d.DepartmentId=p.DepartmentId 
where p.PeopleAddress='武汉'
--2. 查询出武汉地区所有的员工信息，要求显示部门名称，职级名称以及员工的详细资料
SELECT p.*,d.DepartmentName,r.RankName FROM People p
INNER JOIN Department d ON d.DepartmentId=p.DepartmentId 
inner join Rank r on r.RankId=p.RankId
where p.PeopleAddress='武汉'
--3. 根据部门分组统计员工人数，员工工资总和，平均工资，最高工资和最低工资。
select COUNT(PeopleName) 员工人数,SUM(PeopleSalary) 员工工资总和,AVG(PeopleSalary) 平均工资,max(PeopleSalary) 最高工资,MIN(PeopleSalary) 最低工资 from People p
INNER JOIN Department d ON d.DepartmentId=p.DepartmentId 
group by p.DepartmentId
--4. 根据部门分组统计员工人数，员工工资总和，平均工资，最高工资和最低工资，平均工资在10000 以下的不参与统计，并且根据平均工资降序排列。
select COUNT(PeopleName) 员工人数,SUM(PeopleSalary) 员工工资总和,AVG(PeopleSalary) 平均工资,max(PeopleSalary) 最高工资,MIN(PeopleSalary) 最低工资 from People p
INNER JOIN Department d ON d.DepartmentId=p.DepartmentId 
group by p.DepartmentId
having AVG(PeopleSalary)>10000
order by AVG(PeopleSalary) desc
--5. 根据部门名称，然后根据职位名称，分组统计员工人数，员工工资总和，平均工资，最高工资和最低工资
SELECT * FROM People p
group by p.DepartmentId
--6.查询出巨蟹 6.22--7.22 的员工信息
select * from People
where month(PeopleBirth) =6 and day(PeopleBirth) between 22 and 30
or month(PeopleBirth) =7 and day(PeopleBirth) between 1 and 22

--7.查询所有员工信息，添加一列显示属相(鼠,牛,虎,兔,龙,蛇,马,羊,猴,鸡,狗,猪)
select * from Department
select * from Rank
select * ,
case 
when year(PeopleBirth)%12 = 0
then 'zhu'
when year(PeopleBirth)%12 = 1
then '技'
end '123'
from People